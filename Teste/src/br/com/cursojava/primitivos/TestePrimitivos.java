package br.com.cursojava.primitivos;

public class TestePrimitivos {

	public static void main(String[] args) {
		
		int primitivoInteiro = 5;
	
		int primitivoInteiroSemValor = 0;
		
		System.out.println(primitivoInteiro);
		System.out.println(primitivoInteiroSemValor);
		
		char primitivoCaracter = 'a';
		System.out.println(primitivoCaracter);
		
		boolean primitivoBoolean = false;
		System.out.println(primitivoBoolean);
		
		long primitivoLong = 2;
		System.out.println(primitivoLong);
		
		Integer wrapperInteiro = 2;
		System.out.println(wrapperInteiro);
		
		Character wrapperCaracter = 'a';
		System.out.println(wrapperCaracter);
		
		Boolean wrapperBoolean = true;
		System.out.println(wrapperBoolean);
		
		Long wrapperLong = 20l;
		System.out.println(wrapperLong);
		
		System.out.println(wrapperInteiro.getClass());
		
		String[] arrayString = new String[10];
		arrayString[0] = "Curso Java";
		System.out.println(arrayString[0]);
		System.out.println(arrayString[1]);
		System.out.println(arrayString);
		System.out.println(arrayString.toString());
					
	}
	
}
