package br.com.cursojava.exemplo.classe;

import br.com.cursojava.exemplo.classe.abstrata.ExemploClasseAbstrata;

public class ExemploClasse {

	public static void main(String[] args) {
		ExemploClasseComAtributos exemploClasseComAtributos = new ExemploClasseComAtributos();
		exemploClasseComAtributos.cadeiaCaracteres = "";
		exemploClasseComAtributos.umNumero = 0;
		
		//Classe que n�o permite ser instanciada, s� permite ser extendida
		//ExemploClasseAbstrata classeAbstrata = new ExemploClasseAbstrata);
	}

}
