package br.com.cursojava.exemplo.classe.abstrata;

public abstract class ExemploClasseAbstrata {

	//Classe que n�o permite ser instanciada, s� permite ser extendida
	
	public abstract void umMetodoAbstrato(String parametro);
	
}
