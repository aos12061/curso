package br.com.cursojava.exemplo.classe.abstrata.exemplo;

public class TesteExmploDeClasseAbstrata {

	public static void main(String[] args) {
		
		Animal[] AnimalsArray = new Animal[2];
		
		AnimalsArray[0] = new Person();
		AnimalsArray[1] = new Fish();		
		
		AnimalsArray[0].move(0.45f);
		AnimalsArray[1].move(1.5f);
	}
	
}
