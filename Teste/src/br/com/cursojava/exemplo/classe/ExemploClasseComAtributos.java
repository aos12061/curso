package br.com.cursojava.exemplo.classe;

public class ExemploClasseComAtributos {

	//Modificador Default
	String cadeiaCaracteres;
	
	//Modificdor public
	public Integer umNumero;
	
	//Modificador private
	private Long unNumeroLong;
	
	public static void main(String[] args) {
		
		ExemploClasseComAtributos exemploClasseComAtributos = new ExemploClasseComAtributos();
		exemploClasseComAtributos.unNumeroLong = 1l;
		
		
	}
	
	
}
