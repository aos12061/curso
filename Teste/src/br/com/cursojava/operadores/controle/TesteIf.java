package br.com.cursojava.operadores.controle;

public class TesteIf {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		boolean variavelVerificacao = false;
		
		if (variavelVerificacao) {
			System.out.println("Resultado: " + variavelVerificacao);
		} else if (!variavelVerificacao) {
			System.out.println("Resultador else if: " + variavelVerificacao);
		} else {
			System.out.println("Resultado: else: " + variavelVerificacao);
		}
		
	}

}
