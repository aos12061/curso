package br.com.cursojava.operadores.controle;

import br.com.cursojava.operadores.controle.apoio.SwitchEnum;

public class TesteSwitchCase {

	/*
	private static Integer getEnumValue(SwitchEnum enumeravel) {
		
		Integer retorno = null;
		
		switch (enumeravel) {
		
			case CASE_1:
				retorno = enumeravel.getValor() * 40;
				break;
	
			case CASE_2:
				retorno = enumeravel.getValor() * 30;
				break;
				
			case CASE_3:
				retorno = enumeravel.getValor() * 20;
				break;
				
			case CASE_4:
				retorno = enumeravel.getValor() * 10;
				break;
				
			default:
				retorno = enumeravel.getValor() * 1;
				break;
			
		}
		
		return retorno;
		
	}
	
	*/
	
private static String getStringValue(String cadeiaDeCaracter) {
		
		String retorno = null;
		
		switch (cadeiaDeCaracter) {
		
			case "A":
				retorno = "DCBA";
				break;
	
			case "AB":
				retorno = "CBA";
				break;
				
			case "ABC":
				retorno = "BA";
				break;
				
			case "ABCD":
				retorno = "A";
				break;
				
			default:
				retorno = "Valor n�o conhecido!";
				break;
			
		}
		
		return retorno;
		
	}
	
	public static void main(String[] args) {
		/*
		System.out.println(getEnumValue(SwitchEnum.CASE_1));
		System.out.println(getEnumValue(SwitchEnum.CASE_2));
		System.out.println(getEnumValue(SwitchEnum.CASE_3));
		System.out.println(getEnumValue(SwitchEnum.CASE_4));
		System.out.println(getEnumValue(SwitchEnum.CASE_5));
		*/
		
		System.out.println(getStringValue("A"));
		System.out.println(getStringValue("AB"));
		System.out.println(getStringValue("ABC"));
		System.out.println(getStringValue("ABCD"));
		System.out.println(getStringValue("X"));
		
	}
	
}
