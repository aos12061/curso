package br.com.cursojava.conta;

import java.math.BigDecimal;
import java.util.Date;

public abstract class Conta {

	protected double valor;
	protected double saldo;
	
	public BigDecimal getTaxaRendimentos() {
		BigDecimal valor = new BigDecimal(this.valor);
		return valor;
	}

	public boolean realizarLancamento(BigDecimal valor, Date date) {		
		return true;		
	}
	
	public boolean validaLancamento() {
		return true;
	}
	
	public BigDecimal getIR() {		
		BigDecimal valor = new BigDecimal(this.valor);
		return valor;
	}
	
	public BigDecimal getSaldo() {
		BigDecimal saldo = new BigDecimal(this.saldo);
		return saldo;
	}

}
